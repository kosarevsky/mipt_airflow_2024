from airflow.models import BaseOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook


class CheckOperator(BaseOperator):

    template_fields = ("exec_date", "check_table")
    ui_color = '#ccccff'

    def __init__(self,
                 exec_date=None,
                 check_table=None,
                 **kwargs):

        self.exec_date = exec_date
        self.check_table = check_table
        self._pg_conn_id = 'results_db'
        self._answers = {
            '202207': (17.5207, 120.8181, 7, 'mww', '2022-07-27', '2023-07-20T12:36:34.994Z'),
            '202208': (-32.7361, -179.0088, 6.6, 'mww', '2022-08-14', '2022-10-28T02:25:54.040Z'),
            '202209': (-6.2944, 146.5038, 7.6, 'mww', '2022-09-10', '2022-11-19T23:02:16.040Z'),
            '202210': (7.6901, -82.3342, 6.7, 'mww', '2022-10-20', '2022-12-31T23:03:41.040Z'),
            '202211': (-19.2881, -172.1471, 7.3, 'mww', '2022-11-11', '2023-01-21T19:27:21.040Z'),
            '202212': (-15.3497, -172.9867, 6.8, 'mww', '2022-12-04', '2023-02-11T23:03:06.040Z'),
            '202301': (-7.0586, 130.009, 7.6, 'mww', '2023-01-09', '2023-04-05T16:41:13.472Z')
        }

        self._columns = ['latitude', 'longitude', 'mag', 'magtype', 'date', 'updated']
        self._query_template = "SELECT * from {table} WHERE month_id = '{month_id}';"
        self._query_template_row_cnt = "SELECT * from {table} WHERE month_id <= '{month_id}';"
        super().__init__(**kwargs)

    def execute(self, context):
        self.log.info(f"Check if execution date is valid and in range. Value of exec_date is '{self.exec_date}'.")
        check_flag = self.exec_date in ('202207', '202208', '202209', '202210', '202211', '202212', '202301')
        if check_flag:
            self.log.info('Check passed.')
        else:
            msg = (
                f"Execution date is not valid: exec_date = {self.exec_date}. "
                "Execution date must be one of the following: "
                "['202207', '202208', '202209', '202210', '202211', '202212', '202301']"
            )
            raise ValueError(msg)

        self.log.info(f"Connecting to results DB using connection '{self._pg_conn_id}'...")
        pg_hook = PostgresHook(postgres_conn_id=self._pg_conn_id)
        pg_connection = pg_hook.get_conn()
        pg_cursor = pg_connection.cursor()
        self.log.info("Connection successful.")
        self.log.info(f"Get result from table {self.check_table} for execution date '{self.exec_date}'.")
        query = self._query_template.format(table=self.check_table, month_id=self.exec_date)
        self.log.info(f"Execution query: {query}")
        pg_cursor.execute(query)
        result = pg_cursor.fetchall()
        self.log.info(f'Successfully fetched results from table {self.check_table}. Checking values...')
        if len(result) > 1:
            msg = (
                f"Got multiple rows for execution_date '{self.exec_date}'. "
                "Results table must contain single row (only one results set) per execution date."
            )
            raise ValueError(msg)
        values = result[0][:-1]
        checks = []
        for col, val, ans in zip(self._columns, values, self._answers[self.exec_date]):
            if val != ans:
                msg = f"Incorrect value in column {col}: value = {val}."
                checks.append(msg)

        if checks:
            msg = (
                f"Following errors are found in results for execution_date = {self.exec_date}:\n" +
                '\n'.join(checks)
            )
            raise ValueError(msg)

        # total row count check:
        query = self._query_template_row_cnt.format(table=self.check_table, month_id=self.exec_date)
        self.log.info(f"Execution query: {query}")
        pg_cursor.execute(query)
        result = pg_cursor.fetchall()
        self.log.info(f'Successfully fetched results from table {self.check_table}. Checking total row count...')
        expected_row_cnt = int(self.exec_date[-2:]) - 6
        if len(result) < expected_row_cnt:
            msg = (
                f"Table {self.check_table} contains wrong number of rows. "
                f"For execution_date '{self.exec_date}' there should be at least {expected_row_cnt} rows in table, "
                f"but query returned {len(result)} rows."
            )
            raise ValueError(msg)

        self.log.info(">>> SUCCESS <<<")
        self.log.info(f"Results for execution_date = {self.exec_date} are correct.")







