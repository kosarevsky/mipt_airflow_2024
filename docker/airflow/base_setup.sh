#!/bin/bash

host=$1
port=$2
user=$3
password=$4

# connection to results db
curl -X POST "http://$host:$port/api/v1/connections" \
-H 'Content-Type: application/json' \
--user "$user:$password" \
-d '{
  "connection_id": "results_db",
  "conn_type": "Postgres",
  "description": "Connection to Results DB (PostgreSQL)",
  "host": "postgres",
  "port": 5432,
  "schema": "usgs",
  "login": "student",
  "password": "8yLECXlNC1pw"
}'

# connection to USGS API
curl -X POST "http://$host:$port/api/v1/connections" \
-H 'Content-Type: application/json' \
--user "$user:$password" \
-d '{
  "connection_id": "usgs_api",
  "conn_type": "HTTP",
  "description": "Connection to USGS Earthquake Catalog API",
  "host": "https://earthquake.usgs.gov/fdsnws/event/1/"
}'
