# connection to results db
airflow connections add --conn-description "Connection to Results DB (PostgreSQL)" \
  --conn-type "Postgres" --conn-host "postgres" --conn-port 5432 --conn-schema "usgs" \
  --conn-login student --conn-password "8yLECXlNC1pw" \
  "results_db"
# connection to USGS API
airflow connections add --conn-description "Connection to USGS Earthquake Catalog API" \
  --conn-type "HTTP" --conn-host "https://earthquake.usgs.gov/fdsnws/event/1/" \
  "usgs_api"
# create student user:
airflow users create -u student -p KwTFJlwpZ3Eq -r User -f student -l student -e student@student.edu


# =============
# crete role:
airflow roles create Student
airflow roles add-perms -a can_read -r DAG:conn_test_dag Student
airflow roles add-perms -a can_edit -r DAG:conn_test_dag Student
airflow roles add-perms -a can_edit -r DAG:conn_test_dag Student

airflow users create -u student -p KwTFJlwpZ3Eq -r student -f student -l student -e student@student.edu


can_edit
can_read
can_create
can_delete
menu_access
clear
set_failed
set_retry
set_running
set_skipped
set_success


# general roles:
can read on Audit Logs,
can read on DAG Code,
can read on DAG Runs,
can read on ImportError,
can read on DAG Warnings,
can read on Task Instances,
can read on Task Logs,
can read on Website,
menu access on Browse,
menu access on DAGs,
menu access on DAG Runs,
menu access on Documentation,
menu access on Docs,
menu access on Task Instances


