#!/bin/bash

host=$1
port=$2
admin_user=$3
admin_password=$4
dag_id=$5

while true; do
  cmd=$(
    curl -s -I -X GET "http://$host:$port/api/v1/dags/$dag_id" \
    -H 'Content-Type: application/json' \
    --user "$admin_user:$admin_password" | grep HTTP | cut -d' ' -f2
  )
  if [ "$cmd" == 200 ]; then
    echo "Exit code = 200"
    break
  fi
  echo "Exit code = 400"
  sleep 5
done