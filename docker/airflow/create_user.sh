#!/bin/bash

host=$1
port=$2
admin_user=$3
admin_password=$4
user_login=$5
user_password=$6
user_first_name=$7
user_last_name=$8
user_email=$9

# create role for user
curl -X POST "http://$host:$port/api/v1/roles" \
-H 'Content-Type: application/json' \
--user "$admin_user:$admin_password" \
-d "{
  \"name\":\"$user_login\",
  \"actions\": [
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"Audit Logs\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"DAG Code\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"DAG Warnings\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"ImportError\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"Task Logs\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"Website\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"XComs\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"Browse\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"DAGs\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"DAG Runs\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"Documentation\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"Docs\"}},
    {\"action\": {\"name\":\"menu_access\"}, \"resource\": {\"name\":\"Task Instances\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"DAG Runs\"}},
    {\"action\": {\"name\":\"can_create\"}, \"resource\": {\"name\":\"DAG Runs\"}},
    {\"action\": {\"name\":\"can_edit\"}, \"resource\": {\"name\":\"DAG Runs\"}},
    {\"action\": {\"name\":\"can_delete\"}, \"resource\": {\"name\":\"DAG Runs\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"Task Instances\"}},
    {\"action\": {\"name\":\"can_create\"}, \"resource\": {\"name\":\"Task Instances\"}},
    {\"action\": {\"name\":\"can_edit\"}, \"resource\": {\"name\":\"Task Instances\"}},
    {\"action\": {\"name\":\"can_delete\"}, \"resource\": {\"name\":\"Task Instances\"}},
    {\"action\": {\"name\":\"can_read\"}, \"resource\": {\"name\":\"DAG:${user_login}_dag\"}},
    {\"action\": {\"name\":\"can_create\"}, \"resource\": {\"name\":\"DAG:${user_login}_dag\"}},
    {\"action\": {\"name\":\"can_edit\"}, \"resource\": {\"name\":\"DAG:${user_login}_dag\"}},
    {\"action\": {\"name\":\"can_delete\"}, \"resource\": {\"name\":\"DAG:${user_login}_dag\"}}
  ]
}"

# create user
curl -X POST "http://$host:$port/api/v1/users" \
-H 'Content-Type: application/json' \
--user "$admin_user:$admin_password" \
-d "{
  \"first_name\": \"$user_first_name\",
  \"last_name\": \"$user_last_name\",
  \"username\": \"$user_login\",
  \"email\": \"$user_email\",
  \"password\": \"$user_password\",
  \"roles\": [{\"name\":  \"$user_login\"}]
}"