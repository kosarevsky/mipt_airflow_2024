#!/bin/bash

command=$1

if [[ "$command" == "start" ]]; then
  docker-compose up -d --build
elif [[ "$command" == "stop" ]]; then
  docker-compose down
elif [[ "$command" == "restart" ]]; then
  docker-compose down
  docker-compose up -d --build
elif [[ "$command" == "reset" ]]; then
  # Remove all airflow data
  docker-compose down
  rm -r ./dags
  rm -r ./logs
  rm -rf ./pgdata
  rm -r ./plugins
elif [[ "$command" == "clear" ]]; then
  # /!\ WARNING: CLEARS EVERYTHING!
  # Remove all containers/networks/volumes/images and data in db
  docker-compose down
  docker system prune -f
  docker volume prune -f
  docker network prune -f
  rm -r ./dags/*
  rm -r ./logs/*
  rm -rf ./pgdata/*
  # rm -r ./plugins/*
else
  echo "Command $command is not recognized"
  exit 1
fi
