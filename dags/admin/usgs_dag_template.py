# from datetime import datetime
# from airflow.models import DAG
# from operators.check_operator import CheckOperator
#
# RESULTS_TABLE = '<login>'
#
# default_args = {
#     'owner': '<login>',
#     'email': '<email>',
#     'start_date': datetime(2022, 7, 1),
# }
#
# with DAG(
#     dag_id='<login>_dag',
#     default_args=default_args
# ) as main_dag:
#
#     # write your tasks here
#
#     check_results = CheckOperator(
#         task_id='check_results',
#         exec_date='<pass current month here>',
#         check_table=RESULTS_TABLE
#     )
#
#     # uncomment and setup task dependencies:
#     # '<your_tasks>' >> check_results
