import requests
import logging
from io import StringIO
from datetime import datetime
import pandas as pd

from airflow.models import DAG
from airflow.hooks.base import BaseHook
from airflow.operators.python import PythonOperator
from airflow.sensors.python import PythonSensor
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.providers.postgres.operators.postgres import PostgresOperator
from operators.check_operator import CheckOperator


def get_rows_count(**kwargs):
    conn = BaseHook.get_connection(kwargs['conn_id'])
    url = f"{conn.host}{kwargs['method']}?starttime={kwargs['start_date']}&endtime={kwargs['end_date']}"
    logging.info(f'Sending GET request to API url {url}...')
    resp = requests.get(url)
    if resp.status_code == 200:
        logging.info(f'GET request successful. Number of rows for given period is {resp.text}.')
        return int(resp.text) >= 0
    else:
        return False


def get_earthquake_data(**kwargs):
    month_id = kwargs['month_id']
    conn = BaseHook.get_connection(kwargs['api_conn_id'])
    url = (
        conn.host + kwargs['method'] + '?format=' + kwargs['format'] +
        '&starttime=' + kwargs['start_date'] + '&endtime=' + kwargs['end_date']
    )
    logging.info(f'Sending GET request to API url {url}...')
    resp = requests.get(url)
    if resp.status_code == 200:
        logging.info('GET request successful. Processing data...')
        data = StringIO(resp.text)
        df = pd.read_csv(data)
        df_flt = df.loc[(df['type'] == 'earthquake') & (df['mag'].notnull())]
        df_flt['date'] = df_flt['time'].str[:10]
        df_flt['drank'] = df_flt['mag'].rank(method='dense', ascending=False).astype(int)
        res = df_flt.loc[df_flt['drank'] == 1][[
            'latitude', 'longitude', 'mag', 'magType', 'date', 'updated'
        ]]
        res['month_id'] = month_id
        logging.info('USGS data for processed successfully. Writing following result into DB:')
        logging.info(res.to_string().strip())

        logging.info(f"Connecting to results DB using connection: {kwargs['results_conn_id']}")
        pg_hook = PostgresHook(postgres_conn_id=kwargs['results_conn_id'])
        pg_connection = pg_hook.get_conn()
        pg_cursor = pg_connection.cursor()
        logging.info("Connection successful.")

        query_template = kwargs['insert_sql_template']
        final_data = res.to_dict(orient='records')
        for record in final_data:
            query = query_template.format(
                tbl_name=kwargs['results_table'],
                latitude=record['latitude'],
                longitude=record['longitude'],
                mag=record['mag'],
                magType=record['magType'],
                date=record['date'],
                updated=record['updated'],
                month_id=record['month_id']
            )
            logging.info('Executing query:')
            logging.info(query)
            pg_cursor.execute(query)

            pg_connection.commit()
            count = pg_cursor.rowcount
            logging.info(f"{count} record inserted successfully.")

        logging.info('Results are written into DB successfully.')
        pg_cursor.close()
        pg_connection.close()
        logging.info('Connection to Results DB closed successfully.')
    else:
        raise ValueError(f'Failure on getting data from API url {url}...')


RESULTS_TABLE = "USGS_TEST"

CREATE_SQL = """
    create table if not exists {tbl_name}(
        latitude DOUBLE PRECISION,
        longitude DOUBLE PRECISION,
        mag DOUBLE PRECISION,
        magType TEXT,
        date TEXT,
        updated TEXT,
        month_id TEXT,
        unique(month_id)
    );
""".format(tbl_name=RESULTS_TABLE)

INSERT_SQL_TEMPLATE = """
    INSERT INTO {tbl_name} (latitude, longitude, mag, magType, date, updated, month_id)
    VALUES ({latitude}, {longitude}, {mag}, '{magType}', '{date}', '{updated}', '{month_id}')
    ON CONFLICT (month_id) DO UPDATE
    SET latitude = EXCLUDED.latitude,
        longitude = EXCLUDED.longitude,
        mag = EXCLUDED.mag,
        magType = EXCLUDED.magType,
        date = EXCLUDED.date,
        updated = EXCLUDED.updated;
"""

default_args = {
    'owner': 'Airflow',
    'email_on_retry': False,
    'email_on_failure': True,
    'email': 'some-user@some-host.com',
    'start_date': datetime(2022, 7, 1),
    'end_date': datetime(2023, 1, 1),
    'retries': 1,
    'retry_delay': 60,  # seconds
    'depends_on_past': False
}

with DAG(
    dag_id='usgs_dag',
    default_args=default_args,
    schedule_interval='@monthly',
    description="Загрузка данных землетрясений с API USGS за второе полугодие 2022",
    tags=['example', 'practice'],
    max_active_runs=1,
    concurrency=1,
    user_defined_macros={
        'get_month_id': lambda dt: dt.strftime('%Y%m'),
        'get_month_start': lambda dt: dt.replace(day=1).strftime('%Y-%m-%d'),
        'get_month_end': lambda dt: (
                dt.replace(month=dt.month+1, day=1).strftime('%Y-%m-%d')
                if dt.month < 12 else
                dt.replace(year=dt.year+1, month=1, day=1).strftime('%Y-%m-%d')
        ),
    }
) as main_dag:

    # 1) Get month first and last dates:
    MONTH_ID = "{{ get_month_id(execution_date) }}"
    START_DATE = "{{ get_month_start(execution_date) }}"
    END_DATE = "{{ get_month_end(execution_date) }}"

    # Create table to store results (if not exists)
    create_table = PostgresOperator(
        task_id='create_results_table',
        postgres_conn_id='results_db',
        sql=CREATE_SQL
    )

    # 2) Check if data for current execution date is available
    sensor_task = PythonSensor(
        task_id='check_if_data_available',
        python_callable=get_rows_count,
        op_kwargs={
            'conn_id': 'usgs_api',
            'method': 'count',
            'start_date': START_DATE,
            'end_date': END_DATE
        },
        poke_interval=60,
        timeout=300,
        soft_fail=True,
        mode='reschedule'
    )

    # 3) Get data from API and write it into RESULTS TABLE
    load_data_task = PythonOperator(
        task_id='get_data',
        python_callable=get_earthquake_data,
        op_kwargs={
            'api_conn_id': 'usgs_api',
            'results_conn_id': 'results_db',
            'method': 'query',
            'format': 'csv',
            'start_date': START_DATE,
            'end_date': END_DATE,
            'month_id': MONTH_ID,
            'results_table': RESULTS_TABLE,
            'insert_sql_template': INSERT_SQL_TEMPLATE
        }
    )

    check_results = CheckOperator(
        task_id='check_results',
        exec_date=MONTH_ID,
        check_table=RESULTS_TABLE
    )

    create_table >> sensor_task >> load_data_task >> check_results
