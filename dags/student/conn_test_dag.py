from datetime import datetime

from airflow.models import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

default_args = {
    'start_date': datetime(2023, 1, 10),
    'end_date': datetime(2023, 1, 11),
    'owner': 'Student',
    'email': 'some-user@some-host.com',
    'email_on_retry': False,
    'email_on_failure': True,
    'retries': 1,
    'retry_delay': 60,  # seconds
    'depends_on_past': False
}

with DAG(
    dag_id='conn_test_dag',
    default_args=default_args,
    schedule_interval='@once',
    description='Test Dag',
    tags=['TEST'],
    max_active_runs=1,
    concurrency=1,
) as main_dag:

    start = DummyOperator(task_id='start')
    end = DummyOperator(task_id='end')

    create_table = PostgresOperator(
        task_id='create_results_table',
        postgres_conn_id='results_db',
        sql="""
            create table student(
                id uuid NOT NULL,
                val1 varchar(255) NOT NULL,
                val2 varchar(255) not null,
                flag1 bool NOT null default true
            )
        """
    )

    start >> create_table >> end