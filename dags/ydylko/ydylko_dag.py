from datetime import datetime

from airflow.models import DAG
from airflow.operators.dummy import DummyOperator

default_args = {
    'start_date': datetime(2023, 1, 10),
    'end_date': datetime(2023, 1, 11),
    'owner': 'ydylko',
    'email': 'ydylko@university.edu',
    'email_on_retry': False,
    'email_on_failure': False,
    'retries': 1,
    'retry_delay': 60,  # seconds
    'depends_on_past': False
}

with DAG(
    dag_id='ydylko_dag',
    default_args=default_args,
    schedule_interval='@once',
    description='Test Dag for user ydylko',
    tags=['ydylko', 'test', 'dummy'],
    max_active_runs=1,
    concurrency=1,
) as main_dag:

    start = DummyOperator(task_id='start')
    work = DummyOperator(task_id='do_some_work')
    end = DummyOperator(task_id='end')

    start >> work >> end
